package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Objects;

public class NewBookController {
    @FXML
    TextField book_id;

    @FXML
    TextField book_name;

    @FXML
    TextField book_writer;

    @FXML
    TextField book_edition;

    @FXML
    TextField book_publisher;

    @FXML
    TextField book_page;

    @FXML
    TextField book_price;

    public void backToHome(ActionEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/home.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addNewBook(){
        Connection connection;
        PreparedStatement statement;

        String sql = "insert into Books values(?,?,?,?,?,?,?)";
        try{
            connection = JDBCConnect.ConnectDB();
            statement = Objects.requireNonNull(connection).prepareStatement(sql);
            statement.setString(1, book_id.getText());
            statement.setString(2, book_name.getText());
            statement.setString(3, book_writer.getText());
            statement.setString(4, book_edition.getText());
            statement.setString(5, book_publisher.getText());
            statement.setString(6, book_page.getText());
            statement.setString(7, book_price.getText());
            statement.execute();

            statement.close();

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText("New book added");
            alert.showAndWait();
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Check internet connection");
            alert.showAndWait();
        }
    }
}
