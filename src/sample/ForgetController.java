package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;

public class ForgetController {

    @FXML
    TextField username_for;

    @FXML
    TextField surname_for;

    @FXML
    TextField name_for;

    @FXML
    TextField question_for;

    @FXML
    TextField answer_for;

    @FXML
    TextField password_for;



    public void searchUser(){
        Connection connection = JDBCConnect.ConnectDB();
        ResultSet resultSet;
        Statement statement;

        String searchText = username_for.getText();
        String sql = "select * from Account where username='" + searchText +"'";
        try{
            statement = Objects.requireNonNull(connection).createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()){
                surname_for.setText(resultSet.getString(3));
                name_for.setText(resultSet.getString(2));
                question_for.setText(resultSet.getString(6));
                resultSet.close();
                statement.close();
            }else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("User not exists");
                alert.showAndWait();
            }
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Database connection error");
            alert.showAndWait();
        }
    }

    public void retrivePassword(){
        Connection connection = JDBCConnect.ConnectDB();
        ResultSet resultSet;
        Statement statement;

        String sql = "select * from Account where username='" + username_for.getText() + "' and answer='" + answer_for.getText() + "'";
        try {
            statement = Objects.requireNonNull(connection).createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()){
                password_for.setText(resultSet.getString(5));
                resultSet.close();
                statement.close();
            }else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Answer is wrong");
                alert.showAndWait();
            }
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Database connection error");
            alert.showAndWait();
        }
    }

    public void goBackLogin(ActionEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/opening.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
