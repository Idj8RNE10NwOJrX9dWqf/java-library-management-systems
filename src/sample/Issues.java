package sample;

public class Issues {
    private  int issue_id;
    private int student_id;
    private String student_name;
    private String student_surname;
    private int book_id;
    private String book_name;
    private String issue_condition;
    private String issue_date;

    public Issues(int issue_id, int student_id, String student_name, String student_surname, int book_id, String book_name, String issue_condition, String issue_date) {
        this.issue_id = issue_id;
        this.student_id = student_id;
        this.student_name = student_name;
        this.student_surname = student_surname;
        this.book_id = book_id;
        this.book_name = book_name;
        this.issue_condition = issue_condition;
        this.issue_date = issue_date;
    }

    //something

    public int getIssue_id() {
        return issue_id;
    }

    public void setIssue_id(int issue_id) {
        this.issue_id = issue_id;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getStudent_surname() {
        return student_surname;
    }

    public void setStudent_surname(String student_surname) {
        this.student_surname = student_surname;
    }

    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    public String getIssue_condition() {
        return issue_condition;
    }

    public void setIssue_condition(String issue_condition) {
        this.issue_condition = issue_condition;
    }

    public String getIssue_date() {
        return issue_date;
    }

    public void setIssue_date(String issue_date) {
        this.issue_date = issue_date;
    }
}
