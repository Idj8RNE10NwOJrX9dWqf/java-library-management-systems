package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;
import java.util.Optional;

public class IssueBookController {

    @FXML
    TextField book_id;

    @FXML
    TextField book_name;

    @FXML
    TextField book_writer;

    @FXML
    TextField book_edition;

    @FXML
    TextField book_publisher;

    @FXML
    TextField book_page;

    @FXML
    TextField book_price;

    @FXML
    TextField std_id;

    @FXML
    TextField std_name;

    @FXML
    TextField std_surname;

    @FXML
    TextField std_course;

    @FXML
    TextField std_faculty;

    @FXML
    TextField std_department;

    @FXML
    TextField std_birthday;

    @FXML
    DatePicker issue_date;


    public void searchBook(ActionEvent event){
        Connection connection = JDBCConnect.ConnectDB();
        ResultSet resultSet;
        Statement statement;

        String sql = "select * from Books where book_id='" + book_id.getText() +"'";
        try{
            statement = Objects.requireNonNull(connection).createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()){
                book_name.setText(resultSet.getString(2));
                book_writer.setText(resultSet.getString(3));
                book_edition.setText(resultSet.getString(4));
                book_publisher.setText(resultSet.getString(5));
                book_page.setText(resultSet.getString(6));
                book_price.setText(resultSet.getString(7));
                resultSet.close();
                statement.close();
            }else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Book not found");
                alert.showAndWait();
            }
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Database connection error");
            alert.showAndWait();
        }
    }

    public void searchStudent(ActionEvent event){
        Connection connection = JDBCConnect.ConnectDB();
        ResultSet resultSet;
        Statement statement;

        String sql = "select * from Students where std_id='" + std_id.getText() +"'";
        try{
            statement = Objects.requireNonNull(connection).createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()){
                std_name.setText(resultSet.getString(2));
                std_surname.setText(resultSet.getString(3));
                std_course.setText(resultSet.getString(4));
                std_faculty.setText(resultSet.getString(5));
                std_department.setText(resultSet.getString(6));
                std_birthday.setText(resultSet.getString(7));
                resultSet.close();
                statement.close();
            }else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Book not found");
                alert.showAndWait();
            }
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Database connection error");
            alert.showAndWait();
        }
    }

    public void issueBook(ActionEvent event){
        Connection connection;
        connection = JDBCConnect.ConnectDB();
        PreparedStatement preparedStatement;

        String sql = "insert into Issue(book_id,student_id,issue_date) values(?,?,?)";

        try {
            preparedStatement = Objects.requireNonNull(connection).prepareStatement(sql);
            preparedStatement.setString(1, book_id.getText());
            preparedStatement.setString(2, std_id.getText());
            preparedStatement.setString(3, issue_date.getValue().toString());
            preparedStatement.execute();

            preparedStatement.close();

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText("A book issued");
            alert.show();
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText("Database connection error");
            alert.show();
        }
    }

    public void backToHome(ActionEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/home.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
