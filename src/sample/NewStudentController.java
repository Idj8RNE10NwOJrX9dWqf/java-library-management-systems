package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Objects;
import java.util.ResourceBundle;

public class NewStudentController implements Initializable {

    @FXML
    TextField student_id;

    @FXML
    TextField student_name;

    @FXML
    TextField student_surname;

    @FXML
    ComboBox<String> student_course;

    @FXML
    ComboBox<String> student_faculty;

    @FXML
    ComboBox<String> student_department;

    @FXML
    DatePicker student_birthday;

    public void addNewStudent(){
        Connection connection;
        PreparedStatement statement;

        String sql = "insert into Students values(?,?,?,?,?,?,?)";

        try{
            connection = JDBCConnect.ConnectDB();
            statement = Objects.requireNonNull(connection).prepareStatement(sql);
            statement.setString(1, student_id.getText());
            statement.setString(2, student_name.getText());
            statement.setString(3, student_surname.getText());
            statement.setString(4, student_course.getSelectionModel().getSelectedItem());
            statement.setString(5, student_faculty.getSelectionModel().getSelectedItem());
            statement.setString(6, student_department.getSelectionModel().getSelectedItem());
            statement.setString(7, student_birthday.getValue().toString());
            statement.execute();

            statement.close();
            connection.close();

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText("New book added");
            alert.showAndWait();
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Check internet connection");
            alert.showAndWait();
        }
    }

    public void backToHome(ActionEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/home.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        student_course.getItems().addAll("1","2","3","4","5");
        student_faculty.getItems().addAll("Engineering", "Industry");
        student_department.getItems().addAll("COMP1", "COMP2", "COMP3", "INS1", "INS2");
    }
}
