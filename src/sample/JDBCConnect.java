package sample;

import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;

import java.sql.Connection;
import java.sql.DriverManager;

public class JDBCConnect {

    public static Connection ConnectDB(){
        try{
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/alialmasli?autoReconnect=true&useSSL=false" , "root", "password");
            return connection;
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("An exception occurred!");
            alert.getDialogPane().setExpandableContent(new ScrollPane(new TextArea("Something")));
            alert.showAndWait();
            return null;
        }
    }
}
