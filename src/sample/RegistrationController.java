package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

public class RegistrationController implements Initializable {

    @FXML
    TextField name_reg;

    @FXML
    TextField surname_reg;

    @FXML
    TextField username_reg;

    @FXML
    TextField password_reg;

    @FXML
    ComboBox<String> combo_reg;

    @FXML
    TextField answer_reg;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        combo_reg.getItems().addAll("Question1","Question2","Question3");
    }

    public void processSignUp(ActionEvent event){
        Connection connection;
        connection = JDBCConnect.ConnectDB();
        PreparedStatement preparedStatement;

        String sql = "insert into Account (name, surname, username, passcode, question, answer) values(?,?,?,?,?,?)";

        try {
            preparedStatement = Objects.requireNonNull(connection).prepareStatement(sql);
            preparedStatement.setString(1, name_reg.getText());
            preparedStatement.setString(2, surname_reg.getText());
            preparedStatement.setString(3, username_reg.getText());
            preparedStatement.setString(4, password_reg.getText());
            preparedStatement.setString(5, combo_reg.getSelectionModel().getSelectedItem());
            preparedStatement.setString(6, answer_reg.getText());
            preparedStatement.execute();

            preparedStatement.close();

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText("New account created");
            Optional<ButtonType> result = alert.showAndWait();
            ButtonType button = result.orElse(ButtonType.CANCEL);
            if (button == ButtonType.OK) {
                goToHome(event);
            } else {
                backToLogin(event);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void backToLogin(ActionEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/opening.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void goToHome(ActionEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/home.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
