package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;
import java.util.ResourceBundle;

public class ReturnBookController implements Initializable {

    @FXML
    TextField student_id;

    @FXML
    TableView issue_tableView;

    @FXML
    TableColumn i_id;

    @FXML
    TableColumn s_id;

    @FXML
    TableColumn s_name;

    @FXML
    TableColumn s_surname;

    @FXML
    TableColumn b_id;

    @FXML
    TableColumn b_name;

    @FXML
    TableColumn i_condition;

    @FXML
    TableColumn i_date;


    public void searchStudent(){
        Connection connection = JDBCConnect.ConnectDB();
        ResultSet resultSet;
        Statement statement;
        ObservableList<Issues> issues;

        String sql = "select Issue.issue_id,Students.std_id,Students.std_name,Students.std_surname,Books.book_id,Books.book_name,Issue.issue_condition," +
                "Issue.issue_date from Issue JOIN Students on (Issue.student_id = Students.std_id) JOIN Books on (Issue.book_id = Books.book_id) where student_id = " + student_id.getText();
        try{
            issue_tableView.getItems().clear();
            statement = Objects.requireNonNull(connection).createStatement();
            resultSet = statement.executeQuery(sql);
            i_id.setCellValueFactory(new PropertyValueFactory<>("issue_id"));
            s_id.setCellValueFactory(new PropertyValueFactory<>("student_id"));
            s_name.setCellValueFactory(new PropertyValueFactory<>("student_name"));
            s_surname.setCellValueFactory(new PropertyValueFactory<>("student_surname"));
            b_id.setCellValueFactory(new PropertyValueFactory<>("book_id"));
            b_name.setCellValueFactory(new PropertyValueFactory<>("book_name"));
            i_condition.setCellValueFactory(new PropertyValueFactory<>("issue_condition"));
            i_date.setCellValueFactory(new PropertyValueFactory<>("issue_date"));
                while (resultSet.next()){
                    System.out.println(resultSet.getInt(2) +" "+ resultSet.getString(3) +" "+ resultSet.getString(4));
                    issue_tableView.getItems().add(new Issues(resultSet.getInt(1),resultSet.getInt(2),resultSet.getString(3),
                            resultSet.getString(4),resultSet.getInt(5),resultSet.getString(6),resultSet.getString(7),resultSet.getString(8)));
                }
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Database connection error");
            alert.showAndWait();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
