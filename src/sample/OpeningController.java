package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.event.ActionEvent;

import java.sql.*;
import java.util.Objects;

public class OpeningController {

    @FXML
    TextField username_login;

    @FXML
    PasswordField password_login;

    public void processLogin(ActionEvent event){
        Connection connection;
        ResultSet resultSet;
        connection = JDBCConnect.ConnectDB();
        Statement statement;

        String sql = "select * from Account where username='" + username_login.getText() + "' and passcode='"+ password_login.getText()+"'";
//        String sql = "select * from Account where username=? and passcode=?";

        try {
            statement = Objects.requireNonNull(connection).createStatement();
//            preparedStatement.setString(0, username_login.getText());
            resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                resultSet.close();
                statement.close();
                goToHome(event);
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("User not exists");
                alert.showAndWait();
            }
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Check internet connection");
            alert.showAndWait();
        }

    }

    private void goToHome(ActionEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/home.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void goToRegistration(ActionEvent event) {
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/registration.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void goToForget(MouseEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/forget.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
