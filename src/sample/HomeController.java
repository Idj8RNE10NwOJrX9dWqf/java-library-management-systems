package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;


public class HomeController {

    public void logOut(){
        System.out.println("Log out");
    }

    public void goToNewBook(MouseEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/newbook.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void goToStatistics(MouseEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/statistics.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void goToNewStudent(MouseEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/newstudent.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void goToIssueBook(MouseEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/issuebook.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void goToReturnBook(MouseEvent event){
        try{
            Parent root1 = FXMLLoader.load(getClass().getResource("xmls/returnbook.fxml"));
            Scene scene = new Scene(root1);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.centerOnScreen();
            window.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
